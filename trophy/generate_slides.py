import os

OUTPUT='trophy_logos/trophy.tex'

preamble=r"""
\documentclass{beamer}

\usepackage{graphicx}
\usepackage[export]{adjustbox}

\setbeamertemplate{navigation symbols}{}

\title{PVK Informatik I -- Trophy}
\author{Alexander Schoch}
\date{Logos}

\begin{document}
  \begin{frame}
    \maketitle
  \end{frame}
"""

slide=r"""
  \begin{frame}{Logo CTR}
    \begin{figure}
        \centering
        \includegraphics[max width=\linewidth, max height=.9\textheight, height=.9\textheight, keepaspectratio]{../logos/FILE}
    \end{figure}
  \end{frame}
"""

slidesoltitle=r"""
  \begin{frame}
    \begin{center}
      \Huge Lösungen
    \end{center}
  \end{frame}
"""

slidesol=r"""
  \begin{frame}{Logo CTR}
    \begin{figure}
      \begin{minipage}{.66\linewidth}
        \includegraphics[max width=\linewidth, max height=.8\textheight, height=.8\textheight, keepaspectratio]{../logos/FILE}
      \end{minipage}
      \hfill
      \pause
      \begin{minipage}{.32\linewidth}
        \caption{COUNTRY}
      \end{minipage}
    \end{figure}
  \end{frame}
"""

files = os.listdir('logos')
files.sort()

with open(OUTPUT, 'w') as f:
    f.write(preamble)
    for i in files:
        ctr = i.split("_")[0]
        f.write(slide.replace("CTR", ctr).replace("FILE", i))
    
    f.write(slidesoltitle)
    for i in files:
        ctr = i.split("_")[0]
        country = i.split("_")[1].split(".")[0]
        f.write(slidesol.replace("CTR", ctr).replace("FILE", i).replace("COUNTRY", country))

    f.write(r'\end{document}')
