import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}']})

alpha = 0.6
logk = 0.81552

kobs = 4.13e-4 # [s-1]

def linear(x, A0, k):
    return A0 - k * x

pH = np.array([2.5, 6, 8, 13])
logkobs = linear(pH, logk, alpha)

plt.scatter(pH, logkobs, s=7, c='black')
#plt.plot([0,14], linear(np.array([0,14]), logk, alpha))

plt.xlabel(r'pH', fontsize=16)
plt.ylabel(r'$\log_{10} \displaystyle\frac{k_\text{obs}}{\si{\per\second}}$', fontsize=16)

plt.tight_layout()
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
plt.savefig('../plots/phosphinic_acid.pdf')
