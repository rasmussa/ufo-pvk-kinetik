# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt

dt = 0.001
duration = 10
num_points = round(duration / dt)

k1 = 1 
k2 = 0.2

Aarr = np.linspace(0,duration,num_points)
Barr = np.linspace(0,duration,num_points)
Carr = np.linspace(0,duration,num_points)

Aarr[0] = 1
Barr[0] = 0
Carr[0] = 0

for i in np.arange(1, np.size(Aarr)):
    dA = -Aarr[i-1] * k1 * dt
    Aarr[i] = Aarr[i-1] + dA
    dB = Aarr[i-1] * k1 * dt - Barr[i-1] * k2 * dt
    Barr[i] = Barr[i-1] + dB
    dC = Barr[i-1] * k2 * dt
    Carr[i] = Carr[i-1] + dC

plt.plot(np.linspace(0,duration,num_points), Aarr, label=r'A', color='black', linewidth=1)
plt.plot(np.linspace(0,duration,num_points), Barr, linestyle='--', label=r'B', color='black', linewidth=1)
plt.plot(np.linspace(0,duration,num_points), Carr, linestyle='-.', label=r'C', color='black', linewidth=1)


# Axis labels
plt.xlabel(r'$t$ / s', fontsize=16)
plt.ylabel(r'$c$ / \si{\mol\per\liter}', fontsize=16)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('../plots/title_picture.pdf')
