import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}', r'\DeclareSIUnit\permille{\text{\textperthousand}}']})


def lin (x, m, b):
    return m * x + b

def toconc(prom): # [mM]
    return prom * rho_ethanol / M_ethanol

def toprom(conc): # [promille]
    return conc * M_ethanol / rho_ethanol

prom   = 1.2 # [promille] = [uL L-1]
k_prom = -0.12 # [promille/h]

rho_ethanol = 0.78945 # [g mL-1]
M_ethanol   = 46.069 # [g mol-1]

limit = toconc(0.5) # [promille]

c0 = toconc(prom) # [mM]
k  = toconc(k_prom) # [mM h-1]

# generate random data
t = np.linspace(0.5,8,20)
bac = lin(t, k, c0)
tnoise = t + np.random.normal(0,np.abs(k/5),len(t))
bacnoise = bac + np.random.normal(0,np.abs(k/10),len(t))

plt.scatter(tnoise, bacnoise, s=7, color='black')

parms, cov = curve_fit(lin, tnoise, bacnoise)
xarr = np.arange(0,max(t),0.01)
plt.plot(xarr, lin(xarr, *parms), linewidth=1, label='fit', color='black')
plt.plot([min(xarr), max(xarr)], [limit, limit], linewidth=1, linestyle='--', color='black', label=r'\SI{0.5}{\permille}')
print(*parms)
print(toprom(parms))

halfc = parms[1] / 2
t12 = -parms[1] / (2 * parms[0])
print(t12)

plt.plot([0, t12], [halfc, halfc], linewidth=1, color='black', linestyle=':', label=r'$t_{1/2}$')
plt.plot([t12, t12], [0, halfc], linewidth=1, color='black', linestyle=':')

plt.xlabel(r'$t$ / h', fontsize=16)
plt.ylabel(r'$c_\text{EtOH}$ / \si{\milli\molar}', fontsize=16)

plt.legend()
plt.tight_layout()
plt.savefig('../plots/bac.pdf')
