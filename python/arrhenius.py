import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}', r'\usepackage[version=4]{mhchem}']})

Ea = 6e4 # [J mol-1]
k0 = 1 # [s-1]

R = 8.314 # [J mol-1 K-1]

def arrhenius(T, k0, Ea):
    return k0 * np.exp(-Ea / (R * T))

x = np.linspace(1, 3e4, 200)
y = arrhenius(x, k0, Ea)

plt.plot(x, y, color='black', linewidth=1)

plt.xlabel(r'$T$ / \si{\kelvin}', fontsize=16)
plt.ylabel(r'$k$ / \si{\per\second}', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/arrhenius.pdf')
