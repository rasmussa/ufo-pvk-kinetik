import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}']})

k  = 1
A0 = 1

def linear(x, A0, k):
    return A0 + k * x

x = np.linspace(0, 4, 20)
y = 1 / (k * x + 1 / A0)

xnoise = x + np.random.normal(0, np.abs(k / 500), len(x))
ynoise = y + np.random.normal(0, np.abs(k / 500), len(x))

xarr = np.linspace(min(xnoise), max(xnoise), 200)

plt.scatter(xnoise, ynoise, s=7, c='black')

plt.xlabel(r'$t$ / h', fontsize=16)
plt.ylabel(r'$c$ / M', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/determining_order.pdf')
plt.close('all')

# ----- #

ylog = np.log(ynoise)

params, cov = curve_fit(linear, xnoise, ylog)
plt.scatter(xnoise, ylog, s=7, c='black')
plt.plot(xarr, linear(xarr, *params), color='black', linewidth=1)

plt.xlabel(r'$t$ / h', fontsize=16)
plt.ylabel(r'$\ln \displaystyle\frac{c}{\si{\molar}}$', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/determining_order_first.pdf')
plt.close('all')

# ----- #

yinv = 1 / ynoise

params, cov = curve_fit(linear, xnoise, yinv)
plt.scatter(xnoise, yinv, s=7, c='black')
plt.plot(xarr, linear(xarr, *params), color='black', linewidth=1)

plt.xlabel(r'$t$ / h', fontsize=16)
plt.ylabel(r'$\displaystyle\frac{1}{c}$ / \si{\per\molar}', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/determining_order_second.pdf')
plt.close('all')

