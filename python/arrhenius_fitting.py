import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}', r'\usepackage[version=4]{mhchem}']})

R = 8.314 # [J mol-1]
Ea  = 6e3 * R
k0 = 100

def linear(x, m, b):
    return b + m * x

T = np.linspace(300, 350, 20)
x = 1 / T
y = linear(x, -Ea / R, np.log(k0))

xnoise = x #+ np.random.normal(0, np.abs(Ea / R / 1e7), len(x))
ynoise = y + np.random.normal(0, np.abs(Ea / R / 1e5), len(x))

xarr = np.linspace(min(xnoise), max(xnoise), 2)

plt.scatter(xnoise, ynoise, s=7, c='black')

params, cov = curve_fit(linear, xnoise, ynoise)
plt.plot(xarr, linear(xarr, *params), color='black', linewidth=1)

plt.xlabel(r'$1/T$ / \si{\kelvin}', fontsize=16)
plt.ylabel(r'$\ln k$ / \si{\per\second}', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/arrhenius_fit.pdf')
plt.close('all')

# ----- #

x = [xnoise[i] for i in [0, -1]]
y = [ynoise[i] for i in [0, -1]]

plt.scatter(x, y, s=7, c='black')
plt.plot(x, y, color='black', linewidth=1)

plt.text(x[1] - 0.06 * (x[1] - x[0]), y[1], r'$P_1$', verticalalignment='center', horizontalalignment='left', fontsize=16)
plt.text(x[0], y[0] + 0.07 * (y[1] - y[0]), r'$P_2$', horizontalalignment='center', verticalalignment='bottom', fontsize=16)

plt.plot([x[1], x[1]], y, linewidth=1, color='black', linestyle='--')
plt.plot(x, [y[0], y[0]], linewidth=1, color='black', linestyle='--')

plt.text(np.mean(x), y[0] - 0.02 * (y[0] - y[1]), r'$\displaystyle\frac{1}{T_2} - \frac{1}{T_1}$', verticalalignment='bottom', horizontalalignment='center', fontsize=16)
plt.text(x[1] - 0.02 * (x[1] - x[0]), np.mean(y), r'$\displaystyle\ln k_2 - \ln k_1$', verticalalignment='center', horizontalalignment='left', fontsize=16)


plt.xlabel(r'$1/T$ / \si{\kelvin}', fontsize=16)
plt.ylabel(r'$\ln k$ / \si{\per\second}', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/arrhenius_fit_twopoints.pdf')
