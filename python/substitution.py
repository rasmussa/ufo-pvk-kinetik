import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}', r'\usepackage[version=4]{mhchem}']})

k  = 1
A0 = 0


def linear(x, A0, k):
    return A0 + k * x

x = np.linspace(0, 4, 20)
y = linear(x, A0, k)

xnoise = x + np.random.normal(0, np.abs(k / 10), len(x))
ynoise = y + np.random.normal(0, np.abs(k / 10), len(x))

xarr = np.linspace(min(xnoise), max(xnoise), 2)

plt.scatter([0,0], [0, 4], c='white') # stretch both plots the same amount
plt.scatter(xnoise, ynoise, s=7, c='black')

params, cov = curve_fit(linear, xnoise, ynoise)
plt.plot(xarr, linear(xarr, *params), color='black', linewidth=1)

plt.xlabel(r'$[\ce{BnCl}]_0$ / \si{\molar}', fontsize=16)
plt.ylabel(r'$r_0$ / \si{\molar\per\second}', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/substitution_BnCl.pdf')
plt.close('all')

# ----- #

y = linear(x, 1, 0)
ynoise = y + np.random.normal(0, np.abs(k / 10), len(x))

params, cov = curve_fit(linear, xnoise, ynoise)
plt.scatter([0,0], [0, 4], c='white') # stretch both plots the same amount
plt.scatter(xnoise, ynoise, s=7, c='black')
plt.plot(xarr, linear(xarr, *params), color='black', linewidth=1)

plt.xlabel(r'$[\ce{PhS-}]_0$ / \si{\molar}', fontsize=16)
plt.ylabel(r'$r_0$ / \si{\molar\per\second}', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/substitution_PhS.pdf')
plt.close('all')
