# THIS IS A SUPER NASTY HACK

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}', r'\usepackage[version=4]{mhchem}']})

plt.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def gaussian(x, sig, mu):
    return 1 / (sig * np.sqrt(2 * np.pi)) * np.exp(-(x - mu)**2 / (2 * sig**2))

xarr = np.linspace(-10,10,100)
sigmoid = 1 - sigmoid(xarr)
gauss = gaussian(xarr, 2, 0) * 5

yarr = sigmoid + gauss
plt.plot(xarr, yarr, linewidth=1, color='black')

plt.xlabel("Reaktionskoordinate", fontsize=16)
plt.ylabel("$E$", fontsize=16)

plt.text(-10, 1, "Diesel", horizontalalignment='left', verticalalignment='bottom', fontsize=16)
plt.text(10, 0.01, "\ce{CO2} / \ce{H2O}", horizontalalignment='right', verticalalignment='bottom', fontsize=16)

topx = yarr.argmax(axis=0)
topy = (max(yarr) - 1) * 0.9 + 1

mid = (topy + 1) / 2

plt.errorbar(xarr[topx], mid, yerr = (topy - 1)/2, color='black', capsize=5, elinewidth=1)
plt.text(xarr[topx-1], mid, r"$E_\text{a}$", horizontalalignment='right', verticalalignment='center', fontsize=16)

plt.tight_layout()
plt.savefig('../plots/activation_energy.pdf')
