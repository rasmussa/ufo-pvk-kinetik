import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}']})

A0 = 1
k  = 1

def secondorder(x, A0, k):
    return 1 / (k * x + 1/A0)

def linear(x, A0, k):
    return A0 + k * x

x = np.linspace(0, 4, 20)
y = secondorder(x, A0, k)

xnoise = x + np.random.normal(0, np.abs(k / 50), len(x))
ynoise = y + np.random.normal(0, np.abs(k / 50), len(x))

plt.scatter(xnoise, ynoise, s=7, c='black')

params, cov = curve_fit(secondorder, xnoise, ynoise)

xarr = np.linspace(min(xnoise), max(xnoise), 200)
plt.plot(xarr, secondorder(xarr, *params), color='black', linewidth=1, label=r'second order fit')

t12 = 1 / (params[1] * params[0])
print(params)
c12 = params[0] / 2

plt.plot([0, t12], [c12, c12], linestyle=':', linewidth=1, color='black', label=r'$t_{1/2}$')
plt.plot([t12, t12], [0, c12], linestyle=':', linewidth=1, color='black')
plt.plot([0, 2 * t12], [c12 * 2 / 3, c12 * 2 / 3], linestyle='--', linewidth=1, color='black', label=r'$2t_{1/2}$')
plt.plot([t12 * 2, t12 * 2], [0, c12 * 2 / 3], linestyle='--', linewidth=1, color='black')
plt.plot([0, 3 * t12], [c12 / 2, c12 / 2], linestyle='-.', linewidth=1, color='black', label=r'$3t_{1/2}$')
plt.plot([t12 * 3, t12 * 3], [0, c12 / 2], linestyle='-.', linewidth=1, color='black')

plt.xlabel(r'$t$ / s', fontsize=16)
plt.ylabel(r'$c$ / M', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/secondorder.pdf')

plt.close('all')

# ---------------------- #

yinv = 1 / ynoise #- 1 / ynoise[0]

plt.scatter(xnoise, yinv, s=7, c='black')

params, cov = curve_fit(linear, xnoise, yinv)
print(params)
yarr = linear(xarr, *params)

plt.plot(xarr, yarr, color='black', linewidth=1, label=r'fitted linearized plot')

t12 = 1 / (params[1] * params[0])
print(t12)
c12 = linear(t12, *params)

plt.plot([0, t12], [c12, c12], linestyle=':', linewidth=1, color='black', label=r'$t_{1/2}$')
plt.plot([t12, t12], [0, c12], linestyle=':', linewidth=1, color='black')
c12 = linear(t12 * 2, *params)
plt.plot([0, 2 * t12], [c12, c12], linestyle='--', linewidth=1, color='black', label=r'$2t_{1/2}$')
plt.plot([t12 * 2, t12 * 2], [0, c12], linestyle='--', linewidth=1, color='black')
c12 = linear(t12 * 3, *params)
plt.plot([0, 3 * t12], [c12, c12], linestyle='-.', linewidth=1, color='black', label=r'$3t_{1/2}$')
plt.plot([t12 * 3, t12 * 3], [0, c12], linestyle='-.', linewidth=1, color='black')

plt.xlabel(r'$t$ / s', fontsize=16)
plt.ylabel(r'$\displaystyle\frac{1}{c}$ / \si{\per\molar}', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/secondorder_linear.pdf')
