import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}']})

R = 8.314 # [J mol-1 K-1]

alpha = 0.6
pH = 12
k = 6.31 #[M-0.6 s-1]

kobs = k * (10**(-pH))**alpha

Ea = 50000 #[J]

Tinv = np.array([0.001, 0.0015, 0.002, 0.0025, 0.003])

def arrhenius(Ea, Tinv):
    return np.exp(-Ea * Tinv / R)

def linear(x, A0, k):
    return A0 - k * x

arrh = arrhenius(Ea, Tinv)

plt.scatter(Tinv, np.log(arrh), s=7, c='black')
#plt.plot([0,14], linear(np.array([0,14]), logk, alpha))

plt.xlabel(r'$1/T$ / \si{\per\kelvin}', fontsize=16)
plt.ylabel(r'$\ln \displaystyle\frac{k_\text{obs}}{k_0}$', fontsize=16)

plt.tight_layout()
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
plt.savefig('../plots/phosphinic_acid_T.pdf')
