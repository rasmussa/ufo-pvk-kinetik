import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

plt.rc('text', usetex=True)
plt.rcParams["font.family"] = "serif"

plt.rcParams.update({'text.latex.preamble' : [r'\usepackage{siunitx}', r'\DeclareSIUnit\molar{M}']})

A0 = 1
k  = 1

def exponential(x, A0, k):
    return A0 * np.exp(-k * x)

def linear(x, A0, k):
    return A0 - k * x

x = np.linspace(0, 4, 20)
y = exponential(x, A0, k)

xnoise = x + np.random.normal(0, np.abs(k / 50), len(x))
ynoise = y + np.random.normal(0, np.abs(k / 50), len(x))

plt.scatter(xnoise, ynoise, s=7, c='black')

params, cov = curve_fit(exponential, xnoise, ynoise)

xarr = np.linspace(min(xnoise), max(xnoise), 200)
plt.plot(xarr, exponential(xarr, *params), color='black', linewidth=1, label=r'first order fit')

t12 = np.log(2) / params[1]
c12 = params[0] / 2

plt.plot([0, t12], [c12, c12], linestyle=':', linewidth=1, color='black', label=r'$t_{1/2}$')
plt.plot([t12, t12], [0, c12], linestyle=':', linewidth=1, color='black')
plt.plot([0, 2 * t12], [c12 / 2, c12 / 2], linestyle='--', linewidth=1, color='black', label=r'$2t_{1/2}$')
plt.plot([t12 * 2, t12 * 2], [0, c12 / 2], linestyle='--', linewidth=1, color='black')
plt.plot([0, 3 * t12], [c12 / 4, c12 / 4], linestyle='-.', linewidth=1, color='black', label=r'$3t_{1/2}$')
plt.plot([t12 * 3, t12 * 3], [0, c12 / 4], linestyle='-.', linewidth=1, color='black')

plt.xlabel(r'$t$ / s', fontsize=16)
plt.ylabel(r'$c$ / M', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/firstorder.pdf')

plt.close('all')

# ---------------------- #

ylog = np.log(ynoise)

plt.scatter(xnoise, ylog, s=7, c='black')

params, cov = curve_fit(linear, xnoise, ylog)
print(params)
yarr = linear(xarr, *params)


plt.plot(xarr, yarr, color='black', linewidth=1, label=r'fitted linearized plot')

t12 = np.log(2) / params[1]
c12 = linear(t12, *params)

plt.plot([0, t12], [c12, c12], linestyle=':', linewidth=1, color='black', label=r'$t_{1/2}$')
plt.plot([t12, t12], [0, c12], linestyle=':', linewidth=1, color='black')
c12 = linear(t12 * 2, *params)
plt.plot([0, 2 * t12], [c12, c12], linestyle='--', linewidth=1, color='black', label=r'$2t_{1/2}$')
plt.plot([t12 * 2, t12 * 2], [0, c12], linestyle='--', linewidth=1, color='black')
c12 = linear(t12 * 3, *params)
plt.plot([0, 3 * t12], [c12, c12], linestyle='-.', linewidth=1, color='black', label=r'$3t_{1/2}$')
plt.plot([t12 * 3, t12 * 3], [0, c12], linestyle='-.', linewidth=1, color='black')

plt.xlabel(r'$t$ / s', fontsize=16)
plt.ylabel(r'$\ln \displaystyle\frac{c}{\si{\molar}}$', fontsize=16)

plt.tight_layout()
plt.legend()
plt.savefig('../plots/firstorder_linear.pdf')
