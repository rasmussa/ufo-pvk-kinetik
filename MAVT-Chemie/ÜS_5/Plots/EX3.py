
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45

plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

conc = np.array([0.316, 0.274, 0.238, 0.190, 0.146])

time= np.array([0, 39, 80, 140 ,210])

lnconc = np.log(conc)
secondconc = 1/conc

def linear(x, a,b):
    return a*x + b

parms, cov = curve_fit(linear, time,secondconc)

xarr = np.linspace(0,210,2)

plt.scatter(time, secondconc)
plt.plot(xarr, linear(xarr,*parms), color="black", linewidth=0.5)



plt.xlabel(r"time [min]")
plt.ylabel(r"$1/c$ [L/mol]")



plt.savefig("4nd_Order.pdf")


